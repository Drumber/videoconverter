/* Copyright (C) Lars Obrath - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package de.lars.videoconverter;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import android.Manifest;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.yausername.ffmpeg.FFmpeg;
import com.yausername.youtubedl_android.YoutubeDL;
import com.yausername.youtubedl_android.YoutubeDLException;

import org.apache.commons.lang3.StringUtils;

import java.util.Locale;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private RadioGroup radioGroup;
    private RadioButton radioButton;
    private Spinner spinner;
    private ArrayAdapter<CharSequence> formatsAdapter;
    private ProgressBar progressBar;
    private Button btnUpdate;
    private Button btnDownload;
    private Button btnClose;
    private Button btnSettings;
    private ImageButton btnClear;
    private EditText etUrl;
    private TextView tvStatus;
    private CheckBox cbBestQuali;
    private Menu menu;

    private boolean updating = false;
    private boolean darkModeOn = false;
    private String format;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    private SharedPref sharedPref;

    public static MainActivity instance;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        sharedPref = new SharedPref(this);
        this.loadLanguage();
        if(sharedPref.loadDarkModeState()) {
            setTheme(R.style.DarkTheme);
            darkModeOn = true;
        } else {
            setTheme(R.style.AppTheme);
            darkModeOn = false;
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.instance = this;

        this.initViews();
        this.initLibs();
        this.createNotificationChannel();

        this.setLastValues();

        if (!isStoragePermissionGranted()) {
            Toast.makeText(this, "Please grant storage permission!", Toast.LENGTH_LONG).show();
        }

        Intent intent = getIntent();
        String action = intent.getAction();
        if(action != null && action.equalsIgnoreCase(Intent.ACTION_SEND) && intent.hasExtra(Intent.EXTRA_TEXT)) {
            String url = intent.getStringExtra(Intent.EXTRA_TEXT);
            etUrl.setText(url);
        }

        //update yt-dl on first run
        if(sharedPref.isFirstRun()) {
            sharedPref.setFirstRun(false);
            this.updateYoutubeDL();
        }
    }

    @Override
    protected void onDestroy() {
        compositeDisposable.dispose();
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.appbar_menu, menu);

        MenuItem menuItem = menu.findItem(R.id.btnDarkMode);
        if(!darkModeOn) {
            menuItem.setIcon(R.drawable.ic_dark_mode);
        } else {
            menuItem.setIcon(R.drawable.ic_normal_mode);
        }

        this.menu = menu;
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == R.id.btnDarkMode) {
            if(darkModeOn) {
                darkModeOn = false;
                sharedPref.setDarkModeState(false);
                item.setIcon(R.drawable.ic_dark_mode);
            } else {
                darkModeOn = true;
                sharedPref.setDarkModeState(true);
                item.setIcon(R.drawable.ic_normal_mode);
            }
            getWindow().setWindowAnimations(R.style.WindowAnimationTransition);
            this.recreate();
        }
        return super.onOptionsItemSelected(item);
    }

    private void initViews() {
        radioGroup = findViewById(R.id.rgType);
        radioButton = findViewById(R.id.rbAudio);
        cbBestQuali = findViewById(R.id.cbBestQuali);
        spinner = findViewById(R.id.spinnerFormats);
        spinner.setOnItemSelectedListener(this.formatsListener);
        this.updateFormatsAdapter();
        progressBar = findViewById(R.id.progressBar);
        btnUpdate = findViewById(R.id.btnUpdate);
        btnUpdate.setOnClickListener(this);
        btnClose = findViewById(R.id.btnClose);
        btnClose.setOnClickListener(this);
        btnSettings = findViewById(R.id.btnSettings);
        btnSettings.setOnClickListener(this);
        btnClear = findViewById(R.id.btnClear);
        btnClear.setOnClickListener(this);
        btnDownload = findViewById(R.id.btnDownload);
        btnDownload.setOnClickListener(this);
        etUrl = findViewById(R.id.etUrl);
        tvStatus = findViewById(R.id.textViewStatus);
        tvStatus.setMovementMethod(new ScrollingMovementMethod());
    }

    private void initLibs() {
        try {
            YoutubeDL.getInstance().init(getApplication());
            FFmpeg.getInstance().init(getApplication());
        } catch (YoutubeDLException e) {
            Log.e("InitLibs", "failed to initialize youtubedl-android", e);
        }
    }

    private void loadLanguage() {
        setLanguage(sharedPref.getLanguage());
    }

    public void setLanguage(String code) {
        Locale locale = new Locale(code);
        Locale.setDefault(locale);
        Configuration config = getBaseContext().getResources().getConfiguration();
        config.setLocale(locale);
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
        sharedPref.setLanguage(code);
    }

    private void setLastValues() {
        boolean video = sharedPref.isSelectedFormatVideo();
        if(video) {
            radioGroup.check(R.id.rbVideo);
        } else {
            radioGroup.check(R.id.rbAudio);
        }

        checkButton(null);

        Log.d("SETUP", "Last selection: " + video + " " + format);
    }

    public void checkButton(View v) {
        int radioId = radioGroup.getCheckedRadioButtonId();
        radioButton = findViewById(radioId);
        this.updateFormatsAdapter();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnDownload: {
                this.resetStatus();

                if(!isStoragePermissionGranted()) {
                    return;
                }

                if(StringUtils.isBlank(etUrl.getText())) {
                    this.setStatus("Please enter valid URL", true);
                    return;
                }
                boolean video = radioButton.getText().equals("Video");

                sharedPref.setSelectedFormat(video, format);

                Intent serviceIntent = new Intent(this, DownloadService.class);
                serviceIntent.putExtra("url", etUrl.getText().toString());
                serviceIntent.putExtra("format", format);
                serviceIntent.putExtra("video", String.valueOf(video));
                serviceIntent.putExtra("bestQuali", String.valueOf(cbBestQuali.isChecked()));
                serviceIntent.putExtra("isMainActivity", String.valueOf(true));

                startService(serviceIntent);

                break;
            }
            case R.id.btnUpdate: {
                this.resetStatus();
                this.updateYoutubeDL();
                break;
            }
            case R.id.btnClose: {
                this.finish();
                break;
            }
            case R.id.btnClear: {
                etUrl.setText("");
                break;
            }
            case R.id.btnSettings: {
                startActivity(new Intent(this, SettingsActivity.class));
                break;
            }
        }
    }

    private AdapterView.OnItemSelectedListener formatsListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
            format = adapterView.getItemAtPosition(i).toString();
        }

        @Override
        public void onNothingSelected(AdapterView<?> adapterView) {
        }
    };

    private void updateFormatsAdapter() {
        if(radioButton.getText().equals("Video")) {
            formatsAdapter = ArrayAdapter.createFromResource(this, R.array.formats_video, android.R.layout.simple_spinner_item);
            cbBestQuali.setEnabled(false);
            cbBestQuali.setVisibility(View.INVISIBLE);
        } else if(radioButton.getText().equals("Audio")) {
            formatsAdapter = ArrayAdapter.createFromResource(this, R.array.formats_audio, android.R.layout.simple_spinner_item);
            cbBestQuali.setEnabled(true);
            cbBestQuali.setVisibility(View.VISIBLE);
        }
        formatsAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(formatsAdapter);

        String lFormat = sharedPref.getSelectedFormat();
        int pos = formatsAdapter.getPosition(lFormat);
        spinner.setSelection(pos);
    }

    private void updateYoutubeDL() {
        if (updating) {
            Toast.makeText(MainActivity.this, "update is already in progress", Toast.LENGTH_LONG).show();
            return;
        }

        updating = true;
        setStatus("Updating YouTube-DL...", false);
        Disposable disposable = Observable.fromCallable(() -> YoutubeDL.getInstance().updateYoutubeDL(getApplication()))
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(status -> {
                    switch (status) {
                        case DONE:
                            Toast.makeText(MainActivity.this, "update successful", Toast.LENGTH_LONG).show();
                            setStatus("Successfully updated YouTube-DL!", false);
                            break;
                        case ALREADY_UP_TO_DATE:
                            Toast.makeText(MainActivity.this, "already up to date", Toast.LENGTH_LONG).show();
                            setStatus("YouTube-DL is already up to date!", false);
                            break;
                        default:
                            Toast.makeText(MainActivity.this, status.toString(), Toast.LENGTH_LONG).show();
                            setStatus(status.toString(), true);
                            break;
                    }
                    updating = false;
                }, e -> {
                    Toast.makeText(MainActivity.this, "download failed", Toast.LENGTH_LONG).show();
                    setStatus("Download failed!", true);
                    updating = false;
                });
        compositeDisposable.add(disposable);
    }

    public boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                return true;
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                return false;
            }
        } else {
            return true;
        }
    }

    public void setDownloadProgress(int progress) {
        progressBar.setProgress(progress);
    }

    public void resetStatus() {
        tvStatus.setText("");
        tvStatus.setTextColor(R.attr.textcolor);
    }

    public void setStatus(String text, boolean red) {
        tvStatus.setText(text);
        if(red) {
            tvStatus.setTextColor(Color.RED);
        } else {
            if(darkModeOn) {
                tvStatus.setTextColor(getResources().getColor(R.color.textColorDarkMode, null));
            } else {
                tvStatus.setTextColor(getResources().getColor(R.color.textColorNormalMode, null));
            }
        }
    }


    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = "Download progress";
            String description = "Shows the current download progress";
            int importance = NotificationManager.IMPORTANCE_LOW;
            NotificationChannel channel = new NotificationChannel(Downloader.CHANNEL_ID, name, importance);
            channel.setDescription(description);

            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }
}
