/* Copyright (C) Lars Obrath - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package de.lars.videoconverter;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

public class ArgumentsFragment extends Fragment {

    private View view;
    private EditText etOpt, etArg;
    private String option, args;
    private Button btnClear;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.arguments_fragment, container, false);
        initViews();

        if(option != null) {
            etOpt.setText(option);
        }
        if(args != null) {
            etArg.setText(args);
        }
        return view;
    }

    public ArgumentsFragment(String option, String args) {
        this.option = option;
        this.args = args;
    }

    private void initViews() {
        etOpt = view.findViewById(R.id.etOptionAF);
        etArg = view.findViewById(R.id.etArgsAF);
        btnClear = view.findViewById(R.id.btnClearAF);
        btnClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etOpt.setText("");
                etArg.setText("");
            }
        });
    }

    public String getOption() {
        return etOpt.getText().toString();
    }

    public void setOption(String o) {
        etOpt.setText(o);
    }

    public String getArgs() {
        return etArg.getText().toString();
    }

    public void setArg(String a) {
        etArg.setText(a);
    }
}
