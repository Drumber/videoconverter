/* Copyright (C) Lars Obrath - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package de.lars.videoconverter;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import com.yausername.ffmpeg.FFmpeg;
import com.yausername.youtubedl_android.YoutubeDL;
import com.yausername.youtubedl_android.YoutubeDLException;
import com.yausername.youtubedl_android.mapper.VideoInfo;

import java.util.Locale;

public class DownloadOverlay extends AppCompatActivity implements View.OnClickListener {

    private ImageButton btnClose;
    private TextView tvTitle;
    private Button btnDownload;
    private RadioButton radioButton;
    private RadioGroup radioGroup;
    private CheckBox cbBestQuali;
    private Spinner spinner;
    private ArrayAdapter<CharSequence> formatsAdapter;

    private SharedPref sharedPref;
    private String format;
    private String url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        sharedPref = new SharedPref(this);
        this.loadLanguage();
        if(sharedPref.loadDarkModeState()) {
            setTheme(R.style.TransparentDark);
        } else {
            setTheme(R.style.Transparent);
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_download_overlay);

        initLibs();
        initViews();

        setLastValues();

        Intent intent = getIntent();
        String action = intent.getAction();
        if(action != null && action.equalsIgnoreCase(Intent.ACTION_SEND) && intent.hasExtra(Intent.EXTRA_TEXT)) {
            url = intent.getStringExtra(Intent.EXTRA_TEXT);
            new LoadTitle().execute(url);
        }
    }

    public void loadLanguage() {
        String code = sharedPref.getLanguage();
        Locale locale = new Locale(code);
        Locale.setDefault(locale);
        Configuration config = getBaseContext().getResources().getConfiguration();
        config.setLocale(locale);
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
    }

    private class LoadTitle extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... strings) {
            String url = strings[0];
            String title = "";

            try {
                VideoInfo info = YoutubeDL.getInstance().getInfo(url);
                title = info.getTitle();
            } catch (YoutubeDLException e) {
                title = "Error: No title info...";
                e.printStackTrace();
            }
            return title;
        }

        @Override
        protected void onPostExecute(String s) {
            tvTitle.setText(s);
        }
    }

    private void initLibs() {
        try {
            YoutubeDL.getInstance().init(getApplication());
            FFmpeg.getInstance().init(getApplication());
        } catch (YoutubeDLException e) {
            Log.e("InitLibs", "failed to initialize youtubedl-android", e);
        }
    }

    private void initViews() {
        btnClose = findViewById(R.id.btnCloseOv);
        btnClose.setOnClickListener(this);
        btnDownload = findViewById(R.id.btnDownloadOv);
        btnDownload.setOnClickListener(this);
        radioButton = findViewById(R.id.rbAudioOv);
        radioGroup = findViewById(R.id.radioGroupOv);
        cbBestQuali = findViewById(R.id.cbBestQualiOv);
        tvTitle = findViewById(R.id.tvTitleOv);
        spinner = findViewById(R.id.spinnerFormatsOv);
        spinner.setOnItemSelectedListener(this.formatsListener);
        this.updateFormatsAdapter();
    }

    private void setLastValues() {
        boolean video = sharedPref.isSelectedFormatVideo();
        if(video) {
            radioGroup.check(R.id.rbVideoOv);
        } else {
            radioGroup.check(R.id.rbAudioOv);
        }

        checkButtonOv(null);

        Log.d("SETUP", "Last selection: " + video + " " + format);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnCloseOv: {
                finish();
                break;
            }
            case R.id.btnDownloadOv: {
                if(!isStoragePermissionGranted()) {
                    return;
                }
                boolean video = radioButton.getText().equals("Video");

                sharedPref.setSelectedFormat(video, format);

                Intent serviceIntent = new Intent(this, DownloadService.class);
                serviceIntent.putExtra("url", url);
                serviceIntent.putExtra("format", format);
                serviceIntent.putExtra("video", String.valueOf(video));
                serviceIntent.putExtra("bestQuali", String.valueOf(cbBestQuali.isChecked()));
                serviceIntent.putExtra("isMainActivity", String.valueOf(false));

                startService(serviceIntent);

                finish();
                break;
            }
        }
    }


    public void checkButtonOv(View v) {
        int radioId = radioGroup.getCheckedRadioButtonId();
        radioButton = findViewById(radioId);
        this.updateFormatsAdapter();
    }


    private AdapterView.OnItemSelectedListener formatsListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
            format = adapterView.getItemAtPosition(i).toString();
        }

        @Override
        public void onNothingSelected(AdapterView<?> adapterView) {

        }
    };

    private void updateFormatsAdapter() {
        if(radioButton.getText().equals("Video")) {
            formatsAdapter = ArrayAdapter.createFromResource(this, R.array.formats_video, android.R.layout.simple_spinner_item);
            cbBestQuali.setEnabled(false);
            cbBestQuali.setVisibility(View.INVISIBLE);
        } else if(radioButton.getText().equals("Audio")) {
            formatsAdapter = ArrayAdapter.createFromResource(this, R.array.formats_audio, android.R.layout.simple_spinner_item);
            cbBestQuali.setEnabled(true);
            cbBestQuali.setVisibility(View.VISIBLE);
        }
        formatsAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(formatsAdapter);

        String lFormat = sharedPref.getSelectedFormat();
        int pos = formatsAdapter.getPosition(lFormat);
        spinner.setSelection(pos);
    }

    public boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                return true;
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                return false;
            }
        } else {
            return true;
        }
    }
}
