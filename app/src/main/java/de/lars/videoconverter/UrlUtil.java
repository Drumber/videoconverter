package de.lars.videoconverter;

public class UrlUtil {

    /**
     * Remove text before url
     * @param url Input url
     * @return Output url
     */
    public static String exposeUrl(String url) {
        int startIndex = 0;
        if(url.contains("https://")) {
            startIndex = url.indexOf("https://");
        } else if(url.contains("http://")) {
            startIndex = url.indexOf("http://");
        } else {
            // unsupported or invalid url
            return url;
        }
        return url.substring(startIndex, url.length());
    }

}
