package de.lars.videoconverter;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class SharedPref {

    SharedPreferences sharedPreferences;

    public SharedPref(Context context) {
        sharedPreferences = context.getSharedPreferences("settings", Context.MODE_PRIVATE);
    }

    public void setPreference(String key, String s) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, s);
        editor.commit();
    }

    public String getPreference(String key, String dflt) {
        return sharedPreferences.getString(key, dflt);
    }

    public void setDarkModeState(boolean state) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean("DarkMode", state);
        editor.commit();
    }

    public boolean loadDarkModeState() {
        boolean state = sharedPreferences.getBoolean("DarkMode", false);
        return  state;
    }

    public void setFirstRun(boolean b) {
        sharedPreferences.edit().putBoolean("firstrun", b).commit();
    }

    public boolean isFirstRun() {
        return sharedPreferences.getBoolean("firstrun", true);
    }

    public void setDownloadPath(String path) {
        this.setPreference("DownloadPath", path);
    }

    public String getDownloadPath() {
        return this.getPreference("DownloadPath", null);
    }

    public void setYtDlArgs(Set<String> args) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putStringSet("ytdlArgs", args).commit();
    }

    public Set<String> getYtDlArgs() {
        String[] defaultArgs = {"--no-playlist", "--no-mtime", "--add-metadata", "--embed-thumbnail", "--postprocessor-args§-id3v2_version 3", "--metadata-from-title§%(artist)s - %(title)s"};
        Set<String> set = new HashSet<>(Arrays.asList(defaultArgs));
        return sharedPreferences.getStringSet("ytdlArgs", set);
    }

    public void resetYtDlArgs() {
        String[] defaultArgs = {"--no-playlist", "--no-mtime", "--add-metadata", "--embed-thumbnail", "--postprocessor-args§-id3v2_version 3", "--metadata-from-title§%(artist)s - %(title)s"};
        Set<String> set = new HashSet<>(Arrays.asList(defaultArgs));
        setYtDlArgs(set);
    }

    public void setSelectedFormat(boolean video, String format) {
        String s = video + "§" + format;
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("selectedformat", s).commit();
    }

    public String getSelectedFormat() {
        String s = sharedPreferences.getString("selectedformat", "false§MPEG-3");
        return s.substring(s.indexOf("§")+1);
    }

    public boolean isSelectedFormatVideo() {
        String s = sharedPreferences.getString("selectedformat", "false§MPEG-3");
        return Boolean.parseBoolean( s.substring(0, s.indexOf("§")) );
    }

    public void setLanguage(String code) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("language", code).commit();
    }

    public String getLanguage() {
        return this.getPreference("language", "en");
    }

}
