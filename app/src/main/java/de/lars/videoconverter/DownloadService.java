/* Copyright (C) Lars Obrath - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package de.lars.videoconverter;

import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

public class DownloadService extends Service {

    private NotificationManagerCompat notificationManager;
    private NotificationCompat.Builder builder;

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        String url = intent.getStringExtra("url");
        String format = intent.getStringExtra("format");
        boolean video = Boolean.parseBoolean(intent.getStringExtra("video"));
        boolean bestQuali = Boolean.parseBoolean(intent.getStringExtra("bestQuali"));
        boolean isMainActivity = Boolean.parseBoolean(intent.getStringExtra("isMainActivity"));

        initNotification();

        new Downloader(this, url, format, video, bestQuali, builder, notificationManager, isMainActivity, intent);

        return START_NOT_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private void initNotification() {
        Intent notificationIntent = new Intent(this, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);

        notificationManager = NotificationManagerCompat.from(this);
        builder = new NotificationCompat.Builder(this, Downloader.CHANNEL_ID);
        builder.setContentTitle("Prepare Download...")
                .setContentText("")
                .setSmallIcon(R.drawable.ic_download)
                .setContentIntent(pendingIntent);

        int PROGRESS_MAX = 100;
        int PROGRESS_CURRENT = 0;
        builder.setProgress(PROGRESS_MAX, PROGRESS_CURRENT, false);

        startForeground(1, builder.build());
    }
}
