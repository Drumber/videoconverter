/* Copyright (C) Lars Obrath - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package de.lars.videoconverter;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

public class SettingsActivity extends AppCompatActivity implements View.OnClickListener {

    private SharedPref sharedPref;
    private Button btnBrowse, btnCancel, btnSave, btnAdd, btnReset, btnLanguage;
    private EditText etPath;
    private LinearLayout layoutArgs;

    private List<ArgumentsFragment> fragments;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedPref = new SharedPref(this);
        fragments = new ArrayList<>();
        if(sharedPref.loadDarkModeState()) {
            setTheme(R.style.DarkTheme);
        } else {
            setTheme(R.style.AppTheme);
        }
        setContentView(R.layout.activity_settings);

        initViews();

        setValues();
        addArgsFragmentsFromSharedPref();
    }

    private void initViews() {
        btnBrowse = findViewById(R.id.btnBrowseS);
        btnBrowse.setOnClickListener(this);
        btnCancel = findViewById(R.id.btnCancelS);
        btnCancel.setOnClickListener(this);
        btnSave = findViewById(R.id.btnSaveS);
        btnSave.setOnClickListener(this);
        btnAdd = findViewById(R.id.btnAddS);
        btnAdd.setOnClickListener(this);
        btnReset = findViewById(R.id.btnResetS);
        btnReset.setOnClickListener(this);
        btnLanguage = findViewById(R.id.btnLanguage);
        btnLanguage.setOnClickListener(this);
        etPath = findViewById(R.id.etPathS);
        layoutArgs = findViewById(R.id.linearLayoutArgsS);
    }

    private void setValues() {
        String path = sharedPref.getDownloadPath();
        if(path == null) {
            etPath.setText(Downloader.getDownloadLocation().getAbsolutePath());
        } else {
            etPath.setText(path);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnCancelS: {
                finish();
                break;
            }
            case R.id.btnSaveS: {
                save();
                finish();
                break;
            }
            case R.id.btnBrowseS: {
                openPathChooser();
                break;
            }
            case R.id.btnAddS: {
                addArgsFragment(null, null);
                break;
            }
            case R.id.btnResetS: {
                etPath.setText(Downloader.getDownloadLocation().getAbsolutePath());
                sharedPref.resetYtDlArgs();
                layoutArgs.removeAllViews();
                fragments.clear();
                addArgsFragmentsFromSharedPref();
                break;
            }
            case R.id.btnLanguage: {
                showLanguageDialog();
            }
        }
    }

    public void addArgsFragment(String opt, String arg) {
        ArgumentsFragment fragment = new ArgumentsFragment(opt, arg);
        fragments.add(fragment);
        getSupportFragmentManager().beginTransaction().add(layoutArgs.getId(), fragment).commit();
    }

    private void addArgsFragmentsFromSharedPref() {
        Set<String> args = sharedPref.getYtDlArgs();
        for(String s : args) {
            String[] argsSplit = splitYtDlArg(s);
            if(argsSplit.length > 1) {
                addArgsFragment(argsSplit[0], argsSplit[1]);
            } else {
                addArgsFragment(argsSplit[0], null);
            }
        }
    }

    public String[] splitYtDlArg(String s) {
        return s.split("§", 2);
    }

    private void openPathChooser() {
        Intent i = new Intent(Intent.ACTION_OPEN_DOCUMENT_TREE);
        i.addCategory(Intent.CATEGORY_DEFAULT);
        startActivityForResult(Intent.createChooser(i, "Choose directory"), 9999);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        switch (requestCode) {
            case 9999:
                Log.i("DownloadPath", "Result uri: " + getPathFromUri(data.getData()));
                etPath.setText(getPathFromUri(data.getData()));
                break;
        }
    }

    public String getPathFromUri(Uri uri) {
        Uri docUri = DocumentsContract.buildDocumentUriUsingTree(uri, DocumentsContract.getTreeDocumentId(uri));
        String path = "";
        try {
            path = PathUtil.getPath(this, docUri);
        } catch (Exception e) {
            etPath.setHint("ERROR! Please choose another directory.");
        }
        return path;
    }

    public void save() {
        if(etPath.getText().toString() != null && !etPath.getText().toString().isEmpty()) {
            sharedPref.setDownloadPath(etPath.getText().toString());
        } else {
            System.out.println("Download path invalid!");
            sharedPref.setDownloadPath(null);
        }

        Set<String> argsSet = new HashSet<>();
        for(ArgumentsFragment af : fragments) {
            if(!af.getOption().isEmpty()) {
                String opt = af.getOption();
                String arg = af.getArgs();

                if(!arg.isEmpty()) {
                    argsSet.add(opt + "§" + arg);
                } else {
                    argsSet.add(opt);
                }
            }
        }
        sharedPref.setYtDlArgs(argsSet);
    }


    private void showLanguageDialog() {
        String[] langs = {"English", "German"};
        AlertDialog.Builder aBuiler = new AlertDialog.Builder(this);
        aBuiler.setTitle(R.string.changeLanguage);
        aBuiler.setSingleChoiceItems(langs, -1, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                switch (i) {
                    case 0:
                        //english
                        MainActivity.instance.setLanguage("en");
                        break;
                    case 1:
                        //german
                        MainActivity.instance.setLanguage("de");
                        break;
                }
                dialogInterface.dismiss();
            }
        });
        AlertDialog aDialog = aBuiler.create();
        aDialog.show();
    }
}
