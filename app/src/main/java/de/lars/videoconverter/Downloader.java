package de.lars.videoconverter;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import com.yausername.youtubedl_android.DownloadProgressCallback;
import com.yausername.youtubedl_android.YoutubeDL;
import com.yausername.youtubedl_android.YoutubeDLException;
import com.yausername.youtubedl_android.YoutubeDLRequest;
import com.yausername.youtubedl_android.mapper.VideoInfo;

import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Set;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.core.content.FileProvider;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class Downloader {

    public final static String CHANNEL_ID = "CURRENT_PROGRESS";
    private static boolean downloading = false;
    private CompositeDisposable compositeDisposable;
    private Context context;
    private MainActivity mainActivity = MainActivity.instance;
    private VideoInfo streamInfo;
    private NotificationCompat.Builder builder;
    private NotificationManagerCompat notificationManager;
    private SharedPref sharedPref;

    private String url;
    private String format;
    private boolean video;
    private boolean bestQuali;
    private boolean isMainActivity;
    private Intent serviceIntent;
    private String filePath;

    public Downloader(Context context, String url, String format, boolean video, boolean bestQuali, NotificationCompat.Builder builder, NotificationManagerCompat notificationManager, boolean isMainActivity, Intent serviceIntent) {
        compositeDisposable = new CompositeDisposable();
        sharedPref = new SharedPref(context);
        this.context = context;

        this.url = url;
        this.video = video;
        this.format = format;
        this.bestQuali = bestQuali;
        this.builder = builder;
        this.notificationManager = notificationManager;
        this.isMainActivity = isMainActivity;
        this.serviceIntent = serviceIntent;

        // remove text before the url
        this.url = UrlUtil.exposeUrl(this.url);
        System.out.println(this.url);

        if(!startDownload()) {
            context.stopService(serviceIntent);
        }
    }


    private DownloadProgressCallback callback = new DownloadProgressCallback() {
        @Override
        public void onProgressUpdate(float progress, long etaInSeconds) {
            if(isMainActivity) {
                mainActivity.runOnUiThread(() -> {
                            mainActivity.setDownloadProgress((int) progress);
                            builder.setProgress(100, (int) progress, false);
                            notificationManager.notify(1, builder.build());
                            mainActivity.setStatus("\n\n\n" + String.valueOf(progress) + "% (ETA " + String.valueOf(etaInSeconds) + " seconds)", false);
                        }
                );
            } else {
                builder.setProgress(100, (int) progress, false);
                notificationManager.notify(1, builder.build());
            }
        }
    };

    private boolean startDownload() {
        if (downloading) {
            Toast.makeText(context, "cannot start download. a download is already in progress", Toast.LENGTH_LONG).show();
            return false;
        }

        if (StringUtils.isBlank(url)) {
            if(isMainActivity)
                mainActivity.setStatus("\n\n\n" + "Invalid URL!", true);
            return false;
        }

        try {
            streamInfo = YoutubeDL.getInstance().getInfo(url);
        } catch (YoutubeDLException e) {
            e.printStackTrace();
        }

        builder.setContentText(getVideoName());
        builder.setContentTitle("Downloading");
        notificationManager.notify(1, builder.build());

        YoutubeDLRequest request = new YoutubeDLRequest(url);

        String path = sharedPref.getDownloadPath();
        if(path == null) {
            File youtubeDLDir = getDownloadLocation();
            path = youtubeDLDir.getAbsolutePath();
        }
        System.out.println("Download path: " + path);
        request.setOption("-o", path + "/%(title)s.%(ext)s");
        this.setRequestOptions(request);

        if(isMainActivity) {
            mainActivity.setStatus("\n\n\n" + "Starting download....", false);
            mainActivity.setDownloadProgress(0);
        }

        downloading = true;
        Disposable disposable = Observable.fromCallable(() -> YoutubeDL.getInstance().execute(request, callback))
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(youtubeDLResponse -> {
                    if(isMainActivity) {
                        mainActivity.setDownloadProgress(100);
                        mainActivity.setStatus("\n\n\n" + "Download finished!", false);
                    }
                    Toast.makeText(context, "download successful", Toast.LENGTH_LONG).show();
                    downloading = false;

                    this.scanMediaFiles();

                    builder.setContentTitle("Download complete").setProgress(0, 0, false);
                    notificationManager.notify(1, builder.build());

                }, e -> {
                    if(isMainActivity)
                        mainActivity.setStatus("Download failed! " + e.getMessage(), true);
                    Toast.makeText(context, "Download failed", Toast.LENGTH_LONG).show();
                    Log.e("YT-DL", "failed download: " + e.getMessage());
                    e.printStackTrace();
                    downloading = false;
                    context.stopService(serviceIntent);
                });
        compositeDisposable.add(disposable);
        return true;
    }

    private void setRequestOptions(YoutubeDLRequest request) {
        /*request.setOption("-no--playlist");
        request.setOption("--no-mtime");
        request.setOption("--add-metadata");
        request.setOption("--embed-thumbnail");
        request.setOption("--postprocessor-args", "-id3v2_version 3");*/
        setSharePrefOptions(request);

        if(video) {
            int opt = -1;
            switch (format) {
                case "Best":
                    request.setOption("-f", "best");
                    break;
                case "1080p":
                    opt = 1080;
                    break;
                case "720p":
                    opt = 720;
                    break;
                case "480p":
                    opt = 480;
                    break;
                case "360p":
                    opt = 360;
                    break;
            }
            if(opt != -1) {
                request.setOption("-f", "bestvideo[height<=" + opt + "]+bestaudio/best[height<=" + opt +"]");
            }

        } else {
            request.setOption("-x");
            //request.setOption("--metadata-from-title", "%(artist)s - %(title)s");
            String opt = "";
            switch (format) {
                case "MPEG-3":
                    opt = "mp3";
                    break;
                case "wav":
                    opt = "wav";
                    break;
                case "m4a":
                    opt = "m4a";
                    break;
                case "opus":
                    opt = "opus";
                    break;
            }
            if(opt.equals("m4a") && bestQuali) {
                request.setOption("-f", "bestaudio[ext=m4a]");

            } else {
                request.setOption("-f", "bestaudio");
                if(!opt.equals("")) {
                    request.setOption("--audio-format", opt);
                }
                if(bestQuali) {
                    request.setOption("--audio-quality", "1");
                }
            }
        }
    }

    private void setSharePrefOptions(YoutubeDLRequest request) {
        Set<String> args = sharedPref.getYtDlArgs();
        for(Iterator<String> it = args.iterator(); it.hasNext(); ) {
            String s = it.next();
            String[] argsSplit = s.trim().split("§", 2);

            if(argsSplit[0].equalsIgnoreCase("--embed-thumbnail") && video) {
                continue;
            }
            if(argsSplit.length > 1) {
                System.out.println("'" + argsSplit[0] + "' ; '" + argsSplit[1] + "'");
                request.setOption(argsSplit[0], argsSplit[1]);
            } else {
                System.out.println("'" + argsSplit[0] + "'");
                request.setOption(argsSplit[0]);
            }
        }
    }

    @NonNull
    public static File getDownloadLocation() {
        File downloadsDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
        File youtubeDLDir = new File(downloadsDir, "video-converter");
        if (!youtubeDLDir.exists()) youtubeDLDir.mkdir();
        return youtubeDLDir;
    }


    public String getVideoName() {
        if(streamInfo != null) {
            return streamInfo.getTitle();
        }
        return "";
    }


    private void scanMediaFiles() {
        try {
            String name = streamInfo.getTitle();

            String path = sharedPref.getDownloadPath();
            if(path == null) {
                path = getDownloadLocation().getAbsolutePath();
            }

            List<String> files = new ArrayList<String>();
            File[] dir = new File(path).listFiles();

            for(File file : dir) {
                if(file.isFile() && file.getName().contains(name)) {
                    files.add(file.getAbsolutePath());
                }
            }

            if(files.size() > 0) {
                String[] scanFiles = files.toArray(new String[0]);
                MediaScannerConnection.scanFile(context, scanFiles, null, new MediaScannerConnection.OnScanCompletedListener() {
                    @Override
                    public void onScanCompleted(String s, Uri uri) {
                        filePath = s;
                        System.out.println("[VideoConverter] [MediaScanner] Scanned file: " + s);
                        notifyFinish();
                        context.stopService(serviceIntent);
                    }
                });
            }

        } catch (NullPointerException e) {
            System.out.println("[VideoConverter] [MediaScanner] No file found / Error while scanning files!");
        }
    }

    private void notifyFinish() {
        File file = new File(filePath);
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        Uri uri = FileProvider.getUriForFile(context, context.getApplicationContext().getPackageName() + ".fileprovider", file);

        if(video) {
            intent.setDataAndType(uri, "video/*");
        } else {
            intent.setDataAndType(uri, "audio/*");
        }
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);

        NotificationCompat.Builder notification = new NotificationCompat.Builder(context, CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_download)
                .setContentTitle("Download finished")
                .setContentText(getVideoName())
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setAutoCancel(true)
                .setContentIntent(pendingIntent);
        NotificationManagerCompat managerCompat = NotificationManagerCompat.from(context);

        managerCompat.notify((new Random().nextInt()), notification.build());
    }

}
